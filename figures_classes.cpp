#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <functional>
#include <sstream>
#include <vector>

using namespace std;

const double PI = 3.1415926535;

class Figure {
public:
    virtual double get_square() const = 0;
    virtual string to_string() const = 0;
    virtual ~Figure() {};
};

class Circle : public Figure {
    double radius;
public:
    static Circle* make(const string& str) {
        Circle* cure = new Circle;

        std::stringstream ss(str);
        ss >> cure->radius;

        return cure;
    }

    double get_square() const override {
        return PI * radius * radius;
    }

    string to_string() const override {
        ostringstream ss;
        ss << "C" << " " << radius;

        return ss.str();
    }
};

class Square : public Figure {
    double a;
public:
    static Square* make(const string& str) {
        Square* cure = new Square;

        std::stringstream ss(str);
        ss >> cure->a;

        return cure;
    }

    double get_square() const override {
        return a * a;
    }

    string to_string() const override {
        ostringstream ss;
        ss << "S" << " " << a;

        return ss.str();
    }
};

class Rectangle : public Figure {
    double a;
    double b;
public:
    static Rectangle* make(const string& str) {
        Rectangle* cure = new Rectangle;

        std::stringstream ss(str);
        ss >> cure->a >> cure->b;

        return cure;
    }

    double get_square() const override {
        return a * b;
    }

    string to_string() const override {
        ostringstream ss;
        ss << "R" << " " << a << " " << b;

        return ss.str();
    }
};

class FigureFactory {
public:
    static FigureFactory& factory_instance() {
        static FigureFactory i;
        return i;
    }
    
    static Figure* transform(string cure_line) {
        stringstream ss;
        string type;
        
        ss << cure_line;
        ss >> type;
        
        if (type == "R") {
            string num_1, num_2;
            ss >> num_1 >> num_2;
            return Rectangle::make(num_1 + " " + num_2);
        } else if (type == "C") {
            string num_1;
            ss >> num_1;
            return Circle::make(num_1);
        } else if (type == "S") {
            string num_1;
            ss >> num_1;
            return Square::make(num_1);
        }
        
        return nullptr;
    }
};

bool cmp(const Figure* a, const Figure* b) {
    return (a->get_square() < b->get_square());
}

int main() {
    vector<Figure*> figures;
    string cure_line;
    
    while (getline(cin, cure_line)) {
        figures.push_back(FigureFactory::factory_instance().transform(cure_line));
    }
    
    stable_sort(figures.begin(), figures.end(), cmp);
    
    for (auto i = figures.begin(); i != figures.end(); i++) {
        cout << (*i)->to_string() << endl;
        delete *i;
    }
    
    return 0;
}
