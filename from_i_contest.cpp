#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <functional>
#include <sstream>
#include <map>
#include <vector>
#include <cmath>
#include <algorithm>
#include <unordered_map>

using namespace std;

int main() {
    int  n, x;
    long long t;
    
    cin >> n >> x >> t;
    vector<int> answer;
    long long cnt = 0;
    
    vector<pair<int, int> > ice(n);
    
    for (int i = 0; i < n; i++) {
        int cure;
        cin >> cure;
        ice[i] = make_pair(cure, i + 1);
    }
    
    sort(ice.begin(), ice.end());
    
    int end_i = lower_bound(ice.begin(), ice.end(), make_pair(x, 0)) - ice.begin();
    int start_i = end_i - 1;
    
    while (start_i >= 0 || end_i < n) {
        if (start_i >= 0 && end_i < n) {
            if ((ice[end_i].first - x) < (x - ice[start_i].first)) {
                if (t >= ice[end_i].first - x) {
                    t -= (ice[end_i].first - x);
                    answer.push_back(ice[end_i].second);
                    cnt++;
                    end_i++;
                } else {
                    break;
                }
            } else {
                if (t >= (x - ice[start_i].first)) {
                    t -= (x - ice[start_i].first);
                    answer.push_back(ice[start_i].second);
                    cnt++;
                    start_i--;
                } else {
                    break;
                }
            }
        } else if (start_i >= 0) {
            if (t >= (x - ice[start_i].first)) {
                t -= (x - ice[start_i].first);
                answer.push_back(ice[start_i].second);
                cnt++;
                start_i--;
            } else {
                break;
            }
        } else {
            if (t >= (ice[end_i].first - x)) {
                t -= (ice[end_i].first - x);
                answer.push_back(ice[end_i].second);
                cnt++;
                end_i++;
            } else {
                break;
            }
        }
    }
    
    cout << cnt << endl;
    
    sort(answer.begin(), answer.end());
    
    if (cnt != 0) {
        for (auto i : answer) {
            cout << i << " ";
        }
        
        cout << endl;
    }
    
    return 0;
}
