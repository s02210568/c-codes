#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <cstring>
#include <algorithm>

using namespace std;

class Date {
public:
    string date;
    
    Date(const string& tmp) {
        int i = 0;
        
        while (tmp[i] != '/') {
            date += tmp[i];
            i++;
        }
        
        date += "/";
        i++;
        
        int len = 0;
        
        while (tmp[i + len] != '/') {
            len++;
        }
        
        if (len < 2) {
            date += "0";
            date += tmp[i];
            i += 2;
        } else {
            while (tmp[i] != '/') {
                date += tmp[i];
                i++;
            }
            
            i++;
        }
        
        date += "/";
        
        len = 0;
        
        while (i + len < (int)tmp.size()) {
            len++;
        }
        
        if (len < 2) {
            date += "0";
            date += tmp[i];
            i += 2;
        } else {
            while (i < (int)tmp.size()) {
                date += tmp[i];
                i++;
            }
        }
    }
};

int main() {
    map<string, map<string, int> > students;
    set<string> leksik_names;
    set<string> dates;

    string cure_name, cure_date;
    int cure_mark;

    while (cin >> cure_name >> cure_date >> cure_mark) {
        if (students.find(cure_name) == students.end()) {
            map<string, int> cure_map_date;
            cure_map_date.insert(make_pair(Date(cure_date).date, cure_mark));

            students.insert(make_pair(cure_name, cure_map_date));
            leksik_names.insert(cure_name);
        } else {
            students[cure_name][Date(cure_date).date] = cure_mark;
        }

        dates.insert(Date(cure_date).date);
    }

    cout << "." << " ";

    for (auto i : dates) {
        cout << i << " ";
    }

    cout << endl;

    for (auto i : leksik_names) {
        cout << i << " ";
        for (auto j : dates) {
            if (students[i].find(j) == students[i].end()) {
                cout << "." << " ";
            } else {
                cout << students[i][j] << " ";
            }
        }

        cout << endl;
    }
    
    return 0;
}
