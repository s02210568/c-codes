#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <algorithm>
#include <functional>

using namespace std;

template <class it_idx, class it_values>

it_values myremove(it_idx idx_start, it_idx idx_end, it_values values_start, it_values values_end) {
    auto answer = values_start;
    int cure_idx = 0;

    for (auto i = values_start; i != values_end; i++) {
        bool should_del = false;
        auto j = idx_start;
        
        while (j != idx_end) {
            if (cure_idx == *j) {
                should_del = true;
                break;
            }
            
            j++;
        }

        if (!(should_del)) {
            iter_swap(i, answer);
            answer++;
        }
        
        cure_idx++;
    }

    return answer;
}
