#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <list>
#include <cstring>
#include <functional>

using namespace std;

template <class T>

T myfilter(const T &x, function<bool(const typename T::value_type& a)> func) {
    T answer;
    
    for (auto i = x.begin(); i != x.end(); i++) {
        if (func(*i)) {
            answer.insert(answer.end(), *i);
        }
    }
    
    return answer;
}
