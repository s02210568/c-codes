#include <iostream>
#include <list>
#include <vector>
#include <functional>
#include <map>
#include <set>
#include <algorithm>

using namespace std;

template <class it, class func>

void selection_sort(it start, it end, func cmp) {
    for (auto i = start; i != end; i++) {
        it min = i;
        
        for (auto j = i + 1; j != end; j++) {
            if (cmp(*j, *min)) {
                min = j;
            }
        }
        
        if (min != i) {
            iter_swap(min, i);
        }
    }
}

template <class it>

void selection_sort(it start, it end) {
    for (auto i = start; i != end; i++) {
        it min = i;
        
        for (auto j = i + 1; j != end; j++) {
            if (*j < *min) {
                min = j;
            }
        }
        
        if (min != i) {
            iter_swap(min, i);
        }
    }
}
