#include <iostream>
#include <list>
#include <vector>
#include <set>
#include <map>
#include <functional>

using namespace std;

template <class it>

void myapply(it start, it end, function<void(typename iterator_traits<it>::value_type& a)> f) {
    for (auto i = start; i != end; i++) {
        f(*i);
    }
}

template <class it>

vector<reference_wrapper<typename iterator_traits<it>::value_type>> myfilter2(it start, it end, function<bool(typename iterator_traits<it>::value_type& a)> f) {
    vector<reference_wrapper<typename iterator_traits<it>::value_type>> refs;

    for (auto i = start; i != end; i++) {
        if (f(*i)) {
            refs.push_back(*i);
        }
    }

    return refs;
}
