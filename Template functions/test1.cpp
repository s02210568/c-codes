#include <iostream>
#include <cmath>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

template <class T>

typename T::value_type process(const T &x) {
    if (x.size() == 0) {
        return (typename T::value_type());
    }
    
    auto i = x.end();
    i--;
    typename T::value_type sum = *i;
    
    if (x.size() >= 3) {
        i--;
        i--;
        sum += *i;
        
        if (x.size() >= 5) {
            i--;
            i--;
            sum += *i;
        }
    }
    
    return sum;
}
