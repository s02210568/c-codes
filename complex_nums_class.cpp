#include <iostream>
#include <cmath>
#include <iomanip>
#include <string>
#include <sstream>

using std::string;

namespace numbers {
    class complex {
        double real;
        double image;
    public:
        complex(double x = 0, double y = 0) {
            real = x;
            image = y;
        }
        
        explicit complex(const string s) {
            int i = 1;
            int j = 0;
            char num_1[s.length()];
            char num_2[s.length()];
            
            while (s[i] != ',') {
                num_1[j] = s[i];
                j++;
                i++;
            }
            
            j = 0;
            i++;
            
            while (s[i] != ')') {
                num_2[j] = s[i];
                j++;
                i++;
            }
            
            char *eptr = NULL;
            real = strtod(num_1, &eptr);
            image = strtod(num_2, &eptr);
        }
        
        double re() const {
            return real;
        }
        
        double im() const {
            return image;
        }
        
        double abs2() const {
            return real * real + image * image;
        }
        
        double abs() const {
            return sqrt(abs2());
        }
        
        complex operator+=(const complex& num) {
            real += num.real;
            image  += num.image;
            
            return *this;
        }
        
        complex operator-=(const complex& num) {
            real -= num.real;
            image  -= num.image;
            
            return *this;
        }
        
        complex operator*=(const complex& num) {
            double save_old_real = real;
            real = real * num.real - image * num.image;
            image = save_old_real * num.image + image * num.real;
            
            return *this;
        }
        
        complex operator/=(const complex& num) {
            double save_old_real = real;
            real = (real * num.real + image * num.image) / num.abs2();
            image = (image * num.real - save_old_real * num.image) / num.abs2();
            
            return *this;
        }
        
        complex operator-() const {
            return complex(-real, -image);
        }
        
        complex operator~() const {
            return complex(real, -image);
        }
        
        friend complex operator+(const complex& x, const complex& y) {
            complex tmp = x;
            tmp += y;
            
            return tmp;
        }
        
        friend complex operator-(const complex& x, const complex& y) {
            complex tmp = x;
            tmp -= y;
            
            return tmp;
        }
        
        friend complex operator*(const complex& x, const complex& y) {
            complex tmp = x;
            tmp *= y;
            
            return tmp;
        }
        
        friend complex operator/(const complex& x, const complex& y) {
            complex tmp = x;
            tmp /= y;
            
            return tmp;
        }
        
        string to_string() const {
            std::ostringstream strm;
            strm << std::setprecision(10) << "(" << real << "," << image << ")";

            return strm.str();
        }
    };
};
