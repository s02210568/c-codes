#include <iostream>
#include <map>
#include <vector>
#include <functional>
#include <cstring>
#include <set>

using std::string;
using std::vector;
using std::map;
using std::function;
using std::set;

namespace numbers {
    complex eval(const vector<string> &args, const complex &z) {
        complex_stack stack;
        map<string, function<complex_stack(complex_stack& stack)>> operations;

        operations["+"] = [](complex_stack& stack) {
            complex num_1 = +stack;
            stack = ~stack;
            complex num_2 = +stack;
            stack = ~stack;
            stack = stack << (num_1 + num_2);
            
            return stack;
        };
        
        operations["-"] = [](complex_stack& stack) {
            complex num_1 = +stack;
            stack = ~stack;
            complex num_2 = +stack;
            stack = ~stack;
            stack = stack << (num_2 - num_1);
            
            return stack;
        };
        
        operations["*"] = [](complex_stack& stack) {
            complex num_1 = +stack;
            stack = ~stack;
            complex num_2 = +stack;
            stack = ~stack;
            stack = stack << (num_2 * num_1);
            
            return stack;
        };
        
        operations["/"] = [](complex_stack& stack) {
            complex num_1 = +stack;
            stack = ~stack;
            complex num_2 = +stack;
            stack = ~stack;
            stack = stack << (num_2 / num_1);
            
            return stack;
        };
        
        operations["!"] = [](complex_stack& stack) {
            complex num = +stack;
            stack = stack << num;
            
            return stack;
        };
        
        operations[";"] = [](complex_stack& stack) {
            stack = ~stack;
            
            return stack;
        };
        
        operations["~"] = [](complex_stack& stack) {
            complex num = +stack;
            stack = ~stack;
            stack = stack << (~num);
            
            return stack;
        };
        
        operations["#"] = [](complex_stack& stack) {
            complex num = +stack;
            stack = ~stack;
            stack = stack << (-num);
            
            return stack;
        };
        
        size_t size = args.size();
        complex answer;
        set<string> symbs{"+", "-", "*", "/", "!", ";", "~", "#"};
        
        for (size_t i = 0; i < size; i++) {
            if (symbs.find(args[i]) != symbs.end()) {
                stack = operations[args[i]](stack);
            } else {
                if (args[i] == "z") {
                    stack = stack << z;
                } else {
                    complex num(args[i]);
                    stack = stack << num;
                }
            }
            
            answer = +stack;
        }
        
        return answer;
    }
};
