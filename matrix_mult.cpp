#include <iostream>
#include <map>
#include <set>

const uint64_t MOD = 4294967161;

int main() {
    std::map<std::pair<uint64_t, uint64_t>, uint64_t> matrix_1;
    std::map<std::pair<uint64_t, uint64_t>, uint64_t> matrix_all;
    
    std::map <uint64_t, std::set<uint64_t>> colomns_rows;
    uint64_t cure_row, cure_colomn, cure_value;

    while (std::cin >> cure_row >> cure_colomn >> cure_value) {
        if (cure_row == 0 && cure_colomn == 0 && cure_value == MOD) {
            break;
        }
        
        matrix_1[{cure_row, cure_colomn}] = cure_value % MOD;
        colomns_rows[cure_colomn].insert(cure_row);
    }
    
    while (std::cin >> cure_row >> cure_colomn >> cure_value) {
        for (auto &k : colomns_rows[cure_row]) {
            uint64_t tmp = matrix_1[{k, cure_row}] % MOD;
            tmp = ((tmp % MOD) * (cure_value % MOD)) % MOD;
            matrix_all[{k, cure_colomn}] %= MOD;
            matrix_all[{k, cure_colomn}] += tmp % MOD;
            matrix_all[{k, cure_colomn}] %= MOD;
        }
    }
    
    for (auto &ind : matrix_all) {
        if (ind.second != 0) {
            std::cout << ind.first.first << " " << ind.first.second << " " << ind.second % MOD << std::endl;
        }
    }
    
    return 0;
}
